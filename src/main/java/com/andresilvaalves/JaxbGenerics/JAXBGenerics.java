package com.andresilvaalves.JaxbGenerics;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;

public class JAXBGenerics<T> {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;

	public JAXBGenerics(Class<T> clazz) {
		try {
			JAXBContext jc = JAXBContext.newInstance(clazz);
			this.marshaller = jc.createMarshaller();
			this.unmarshaller = jc.createUnmarshaller();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public String marshall(T t, Schema schema) throws JAXBException {
		StringWriter w = new StringWriter();
		
		this.marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		this.marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
		
		if(schema != null){
			this.marshaller.setSchema(schema);
		}		
		this.marshaller.marshal(t, w);		
		return w.toString();
	}

	public T unMarshal(File file,Schema schema)throws JAXBException {
		T resposta = null;
		
		if(schema != null){
	        	this.unmarshaller.setSchema(schema);
		}
		resposta = ((T) this.unmarshaller.unmarshal(file));
		
		return resposta;
	}

	public T unMarshal(String xml, Schema schema)throws JAXBException {
		T resposta = null;
		
		StringReader stringReader = new StringReader(xml);
		StreamSource streamSource = new StreamSource(stringReader);
		
		if(schema != null){
	        	this.unmarshaller.setSchema(schema);
		}
		resposta = ((T) this.unmarshaller.unmarshal(streamSource));
	
		return resposta;
	}

}
