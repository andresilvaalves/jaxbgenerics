package com.andresilvaalves.JaxbGenerics;

import javax.xml.bind.JAXBException;

import com.andresilvaalves.JaxbGenerics.model.Carro;


public class App 
{
    public static void main( String[] args ){
        try {
        	Carro carro = new Carro(1,"VolksWagen","Polo i-motion",2011,2012);
        	
        	JAXBGenerics<Carro> jaxbGenerics = new JAXBGenerics<Carro>(Carro.class);
			String xmlCarro = jaxbGenerics.marshall(carro, null);
			System.out.println(xmlCarro);
			
			Carro carro2 = jaxbGenerics.unMarshal(xmlCarro, null);
			System.out.println(carro2);
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
    }
}
