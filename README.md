JaxbGenerics
============

Esse projeto apresenta uma classe Jaxb de uso generico.

Essa classe pode ser usada para qualquer entidade que possuir @XmlRootElement

exemplo de uso:

        Carro carro = new Carro(1,"VolksWagen","Polo i-motion",2011,2012);          
        JAXBGenerics<Carro> jaxbGenerics = new JAXBGenerics<Carro>(Carro.class);
			  String xmlCarro = jaxbGenerics.marshall(carro, null);
        
